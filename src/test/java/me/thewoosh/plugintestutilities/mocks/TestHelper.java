package me.thewoosh.plugintestutilities.mocks;

import java.util.Random;
import java.util.UUID;

public class TestHelper {

    private static final Random random = new Random();

    public static String createRandomString() {
        return UUID.randomUUID().toString();
    }

    public static String[] createRandomStringArray() {
        return createRandomStringArrayWithRandomLength(125);
    }

    public static String[] createRandomStringArrayWithRandomLength(int bounds) {
        return createRandomStringArray(random.nextInt(bounds));
    }

    public static char createRandomLowerCaseLetter() {
        return (char) ('a' + random.nextInt(26));
    }

    public static char createRandomUpperCaseLetter() {
        return (char) ('A' + random.nextInt(26));
    }

    public static String createRandomLocale() {
        return new String(new char[] {
                createRandomLowerCaseLetter(),
                createRandomLowerCaseLetter(),
                '_',
                createRandomUpperCaseLetter(),
                createRandomUpperCaseLetter()
        });
    }

    public static String[] createRandomStringArray(int size) {
        String[] strings = new String[size];

        for (int i = 0; i < strings.length; i++) {
            strings[i] = createRandomString();
        }

        return strings;
    }

}
