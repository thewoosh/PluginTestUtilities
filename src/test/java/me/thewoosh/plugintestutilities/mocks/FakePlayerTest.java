package me.thewoosh.plugintestutilities.mocks;

import junit.framework.TestCase;

public class FakePlayerTest extends TestCase {

    public void testGetLocale() {
        FakePlayer player = new FakePlayer();

        String locale = player.getLocale();
        assertNotNull(locale);
        assertTrue(locale.equalsIgnoreCase("en_us"));
    }

    public void testSetLocale() {
        FakePlayer player = new FakePlayer();

        for (int i = 0; i < 16; i++) {
            String locale = TestHelper.createRandomLocale();
            player.setLocale(locale);
            assertEquals(player.getLocale(), locale);
        }
    }

}