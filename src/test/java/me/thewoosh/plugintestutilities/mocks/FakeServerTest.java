package me.thewoosh.plugintestutilities.mocks;

import junit.framework.TestCase;

public class FakeServerTest extends TestCase {

    private final FakeServer server = new FakeServer();
    private final FakePlayer player = new FakePlayer();

    public void testAddPlayer() {
        server.getPlayers().clear();
        server.addPlayer(player);
        assertEquals(server.getOnlinePlayers().size(), 1);
        assertTrue(server.getOnlinePlayers().contains(player));
    }

    public void testGetPlayers() {
        FakeServer newServer = new FakeServer();
        assertTrue(newServer.getPlayers().isEmpty());
        assertTrue(newServer.getOnlinePlayers().isEmpty());
        assertEquals(newServer.getPlayers(), newServer.getOnlinePlayers());
    }

    public void testGetItemFactory() {
        assertNotNull(server.getItemFactory());
    }

}