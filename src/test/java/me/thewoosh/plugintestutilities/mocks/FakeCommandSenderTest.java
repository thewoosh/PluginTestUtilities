package me.thewoosh.plugintestutilities.mocks;

import junit.framework.TestCase;

import java.util.UUID;

public class FakeCommandSenderTest extends TestCase {

    private final FakeCommandSender commandSender = new FakeCommandSender();

    public void testSendMessage() {
        String randomString = TestHelper.createRandomString();
        commandSender.sendMessage(randomString);
        assertEquals(commandSender.popMessageSent(), randomString);
    }

    public void testSendMessageStrings() {
        String[] strings = TestHelper.createRandomStringArrayWithRandomLength(12);

        commandSender.sendMessage(strings);

        for (int i = strings.length - 1; i >= 0; i--) {
            assertEquals(strings[i], commandSender.popMessageSent());
        }
    }

    public void testSendMessageUUID() {
        String randomString = TestHelper.createRandomString();
        commandSender.sendMessage(UUID.randomUUID(), randomString);
        assertEquals(commandSender.popMessageSent(), randomString);
    }

    public void testSendMessageUUIDStrings() {
        String[] strings = TestHelper.createRandomStringArrayWithRandomLength(12);

        commandSender.sendMessage(UUID.randomUUID(), strings);

        for (int i = strings.length - 1; i >= 0; i--) {
            assertEquals(strings[i], commandSender.popMessageSent());
        }
    }

}