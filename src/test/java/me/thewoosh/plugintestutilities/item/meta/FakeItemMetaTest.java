package me.thewoosh.plugintestutilities.item.meta;

import com.google.common.collect.Lists;
import me.thewoosh.plugintestutilities.mocks.TestHelper;
import org.bukkit.inventory.meta.ItemMeta;
import org.junit.Test;

import static org.junit.Assert.*;

public class FakeItemMetaTest {

    @Test
    public void testDisplayName() {
        String displayName = TestHelper.createRandomString();

        FakeItemMeta itemMeta = new FakeItemMeta();
        assertFalse(itemMeta.hasDisplayName());

        try {
            itemMeta.getDisplayName();
            fail("hasDisplayName() = false means getDisplayName() throws");
        } catch (Exception ignored) {
        }

        itemMeta.setDisplayName(displayName);
        assertTrue(itemMeta.hasDisplayName());
        assertEquals(displayName, itemMeta.getDisplayName());

        itemMeta.setDisplayName(null);
        assertFalse(itemMeta.hasDisplayName());
    }

    @Test
    public void testClone() {
        ItemMeta meta = new FakeItemMeta();
        meta.setDisplayName(TestHelper.createRandomString());
        meta.setLore(Lists.asList(TestHelper.createRandomString(), new String[] { TestHelper.createRandomString() }));
        meta.setUnbreakable(true);
        meta.setLocalizedName(TestHelper.createRandomString());
        meta.setCustomModelData(12);

        ItemMeta clone = meta.clone();
        assertEquals(meta.getDisplayName(), clone.getDisplayName());
        assertEquals(meta.getLore(), clone.getLore());
        assertEquals(meta.isUnbreakable(), clone.isUnbreakable());
        assertEquals(meta.getLocalizedName(), clone.getLocalizedName());
        assertEquals(meta.getCustomModelData(), clone.getCustomModelData());
    }

}