package me.thewoosh.plugintestutilities.enchantments;

import junit.framework.TestCase;
import org.bukkit.enchantments.Enchantment;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EnchantmentsRegistryTest {

    @Test
    public void firstRegister() {
        // Test passes if the function doesn't throw.
        assertTrue(EnchantmentsRegistry.register());
    }

    @Test
    public void secondAvailability() {
        assertEquals(38, Enchantment.values().length);
    }

}