package me.thewoosh.plugintestutilities.enchantments;

import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.inventory.ItemStack;

public class FakeEnchantment extends Enchantment {

    private final EnchantmentTarget target;

    public FakeEnchantment(String name, EnchantmentTarget target) {
        super(NamespacedKey.minecraft(name));
        this.target = target;
    }

    @Override
    public String getName() {
        return getKey().getKey();
    }

    @Override
    public int getMaxLevel() {
        // Make sure to be as high as needed.
        return 4;
    }

    @Override
    public int getStartLevel() {
        return 0;
    }

    @Override
    public EnchantmentTarget getItemTarget() {
        return target;
    }

    @Override
    public boolean isTreasure() {
        return false;
    }

    @Override
    public boolean isCursed() {
        return false;
    }

    @Override
    public boolean conflictsWith(Enchantment other) {
        return false;
    }

    @Override
    public boolean canEnchantItem(ItemStack item) {
        return true;
    }

}
