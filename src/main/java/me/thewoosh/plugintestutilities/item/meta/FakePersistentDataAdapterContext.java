package me.thewoosh.plugintestutilities.item.meta;

import org.bukkit.persistence.PersistentDataAdapterContext;
import org.bukkit.persistence.PersistentDataContainer;

public class FakePersistentDataAdapterContext implements PersistentDataAdapterContext {

    private static FakePersistentDataAdapterContext instance;

    public static FakePersistentDataAdapterContext getInstance() {
        if (instance == null) {
            instance = new FakePersistentDataAdapterContext();
        }

        return instance;
    }

    private FakePersistentDataAdapterContext() {
        // Singleton
    }

    @Override
    public PersistentDataContainer newPersistentDataContainer() {
        return new FakePersistentDataContainer();
    }

}
