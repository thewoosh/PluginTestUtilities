package me.thewoosh.plugintestutilities.item.meta;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagAdapterContext;
import org.bukkit.inventory.meta.tags.ItemTagType;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of CustomItemTagContainer.
 *
 * WARNING: This is an unsafe class. Exceptions on get() may be throw because of unsafe casts.
 */
@SuppressWarnings("deprecation")
public class FakeCustomItemTagContainer implements CustomItemTagContainer {

    private final Map<NamespacedKey, Object> map = new HashMap<>();

    @Override
    public <T, Z> void setCustomTag(NamespacedKey key, ItemTagType<T, Z> type, Z value) {
        map.put(key, value);
    }

    @Override
    public <T, Z> boolean hasCustomTag(NamespacedKey key, ItemTagType<T, Z> type) {
        return map.containsKey(key);
    }

    @Override
    public <T, Z> Z getCustomTag(NamespacedKey key, ItemTagType<T, Z> type) {
        return (Z) map.get(key);
    }

    @Override
    public void removeCustomTag(NamespacedKey key) {
        map.remove(key);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public ItemTagAdapterContext getAdapterContext() {
        return FakeItemTagAdapterContext.getInstance();
    }

    public void copyFrom(FakeCustomItemTagContainer customTagContainer) {
        this.map.putAll(customTagContainer.map);
    }

}
