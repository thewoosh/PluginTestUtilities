package me.thewoosh.plugintestutilities.item.meta;

import org.bukkit.inventory.meta.tags.CustomItemTagContainer;
import org.bukkit.inventory.meta.tags.ItemTagAdapterContext;

@SuppressWarnings("deprecation")
public class FakeItemTagAdapterContext implements ItemTagAdapterContext {

    private static FakeItemTagAdapterContext instance;

    public static FakeItemTagAdapterContext getInstance() {
        if (instance == null) {
            instance = new FakeItemTagAdapterContext();
        }

        return instance;
    }

    private FakeItemTagAdapterContext() {
        // Singleton
    }

    @Override
    public CustomItemTagContainer newTagContainer() {
        return new FakeCustomItemTagContainer();
    }

}
