package me.thewoosh.plugintestutilities.mocks;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class FakeCommandSender extends CommandSender.Spigot implements CommandSender {

    private final Deque<String> messagesSent = new ArrayDeque<>();
    private final ArrayList<PermissionAttachment> permissionAttachments = new ArrayList<>();

    public Deque<String> getMessagesSent() {
        return messagesSent;
    }

    public String popMessageSent() {
        return messagesSent.pop();
    }

    @Override
    public void sendMessage(String message) {
        messagesSent.push(message);
    }

    @Override
    public void sendMessage(String[] messages) {
        for (String message : messages)
            sendMessage(message);
    }

    @Override
    public void sendMessage(UUID sender, String message) {
        sendMessage(message);
    }

    @Override
    public void sendMessage(UUID sender, String[] messages) {
        sendMessage(messages);
    }

    @Override
    public Server getServer() {
        return Bukkit.getServer();
    }

    @Override
    public String getName() {
        return "FakeCommandSender";
    }

    @Override
    public Spigot spigot() {
        return this;
    }

    @Override
    public boolean isPermissionSet(String name) {
        return false;
    }

    @Override
    public boolean isPermissionSet(Permission perm) {
        return false;
    }

    @Override
    public boolean hasPermission(String name) {
        return false;
    }

    @Override
    public boolean hasPermission(Permission perm) {
        return false;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
        PermissionAttachment attachment = new PermissionAttachment(plugin, this);
        attachment.setPermission(name, value);
        permissionAttachments.add(attachment);
        return attachment;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin) {
        PermissionAttachment attachment = new PermissionAttachment(plugin, this);
        permissionAttachments.add(attachment);
        return attachment;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
        return null;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
        return null;
    }

    @Override
    public void removeAttachment(PermissionAttachment attachment) {

    }

    @Override
    public void recalculatePermissions() {

    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
        return new HashSet<>();
    }

    @Override
    public boolean isOp() {
        return false;
    }

    @Override
    public void setOp(boolean value) {
    }

}
